#!/usr/bin/env bash
# Runs benchmarks against wc -l

SCRIPT_NAME="$0"
SCRIPT_DIR=$(dirname $0)
BIG_FILE_URL="https://s3.amazonaws.com/ggflc/bigfile.txt.gz"
WC="${WC:-wc}"

usage() {
    if [ "$#" -gt 0 ]; then
        >&2 echo "$@"
    fi
    >&2 echo "Usage: ${SCRIPT_NAME} [options] [file]"
    >&2 echo "Fetches a large file (unless one is given) and runs benchmarks against 'wc -l'."
    >&2 echo "    By default, 'ggflc' is taken from the repository directory."
    >&2 echo "      This can be overridden by the GGFLC environment variable."
    >&2 echo "    Additionally, the 'wc' binary which is used can be overridden with"
    >&2 echo "      the WC environment variable."
    >&2 echo ""
    >&2 echo "Available Options:"
    >&2 echo "    -h,--help                         Show this help message"
    >&2 echo "    -n [iters],--num-iters [iters]    Number of iterations to run"
    >&2 echo "                                      during tests (default: 10)"
}

NUM_ITERS=10
TSTFILE=""

while [ "$#" -gt 0 ]; do
    case "$1" in
        -h|--help)
            usage
            exit 1
            ;;
        -n|--num-iters)
            if [ "$#" -lt 2 ]; then
                usage "Missing required argument to $1"
                exit 1
            fi
            NUM_ITERS="$2"
            shift
            shift
            ;;
        -*)
            usage "Unknown option: $1"
            exit 1
            ;;
        *)
            TSTFILE="$1"
            if [ ! -e "$TSTFILE" ]; then
                echo "File does not exist: $1"
                exit 1
            elif [ -d "$TSTFILE" ]; then
                echo "Not a file: $1"
                exit 1
            fi
            shift
            ;;
    esac
done



set -e

if [ -z "$TSTFILE" ]; then
    TSTFILE="${SCRIPT_DIR}/bigfile.txt"
    if [ ! -e "$TSTFILE" ]; then
        if [ -e "$TSTFILE.gz" ]; then
            echo "Unzipping $TSTFILE.gz"
            gunzip "$TSTFILE.gz"
        else
            echo "Downloading $TSTFILE"
            (cd "${SCRIPT_DIR}" && wget "$BIG_FILE_URL" && gunzip "$(basename $TSTFILE).gz")
        fi
    fi
fi

if [ -z "$GGFLC" ]; then
    GGFLC="${SCRIPT_DIR}/ggflc"
    if [ ! -e "$GGFLC" ]; then
        (cd $SCRIPT_DIR && make)
    fi
fi

# Retrieve the total *CPU* time from the output of `time'
get_cpu_time() {
    # Use `tail` instead of AWK pattern just to be safe
    tail -2 | awk '{ TOT += $2 } END { print TOT }'
}

# Time wc on the test file (prints total CPU time)
time_one_wc() {
    command time -p ${WC} -l "$TSTFILE" |& get_cpu_time
}

# Time ggflc on the test file (prints total CPU time)
time_one_ggflc() {
    command time -p ${GGFLC} "$TSTFILE" |& get_cpu_time
}

# Returns the average of all inputs
mean() {
    awk '{ TOT += $0 } END { print (TOT / NR) }'
}

time_wc() {
    for i in `seq $NUM_ITERS`; do
        time_one_wc
    done | mean
}

time_ggflc() {
    for i in `seq $NUM_ITERS`; do
        time_one_ggflc
    done | mean
}

echo "Running benchmarks on test file: $TSTFILE"
echo -n "Mean 'wc -l' CPU time: "
echo "$(time_wc)s"
echo -n "Mean 'ggflc' CPU time: "
echo "$(time_ggflc)s"

