// ggflc --- Gotta Go Fast Line Counter
// (c) Philip Blair && Neil Locketz 2016
// See LICENSE for licensing information

#include <fcntl.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <emmintrin.h>
#include <errno.h>

static inline long long basic_newline_counter(const char *s, long long length) {
  long long i;
  long long count = 0;
  for (i = 0; i < length; ++i) {
    if (s[i] == '\n')
      ++count;
  }
  return count;
}

#ifdef __SSE2__

// Modified from http://stackoverflow.com/a/37932700
static inline int fast_count_newlines_block(const char *s, const int length) {
  int result = 0;
  int inner_length = length;
  int i;
  int j = 0;

  // Points beginning of 4080 elements block.
  const char *s0 = s;

  // Mask of newline characters
  const __m128i mask = _mm_set1_epi8('\n');


  __m128i vsum = _mm_setzero_si128();
  // Outer loop sum result of 4080 sums.
  for (i = 0; i < length; i += 4080) {
    __m128i vsum_uint8 = _mm_setzero_si128(); // 16 uint8 sum elements (each uint8 element can sum up to 255).
    __m128i vh, vl, vhl, vhl_lo, vhl_hi;

    // Points beginning of 4080 elements block.
    s0 = s + i;

    if (i + 4080 <= length) {
      inner_length = 4080;
    }
    else {
      inner_length = length - i;
    }

    // Inner loop - sum up to 4080 (compared) results.
    // Each uint8 element can sum up to 255. 16 uint8 elements can sum up to 255*16 = 4080 (compared) results.
    for (j = 0; j < inner_length-15; j += 16) {
      __m128i vs, v;
      vs = _mm_loadu_si128((__m128i *)&s0[j]);  // load 16 chars from input
      v = _mm_cmpeq_epi8(vs, mask);             // compare - set to 0xFF where equal, and 0 otherwise.

      // Consider this: (char)0xFF = (-1)
      vsum_uint8 = _mm_sub_epi8(vsum_uint8, v); // Subtract the comparison result - subtract (-1) where equal.
    }

    vh = _mm_unpackhi_epi8(vsum_uint8, _mm_setzero_si128());        // unpack result into 2 x 8 x 16 bit vectors
    vl = _mm_unpacklo_epi8(vsum_uint8, _mm_setzero_si128());
    vhl = _mm_add_epi16(vh, vl);    // Sum high and low as uint16 elements.

    vhl_hi = _mm_unpackhi_epi16(vhl, _mm_setzero_si128());   // unpack sum of vh an vl into 2 x 4 x 32 bit vectors
    vhl_lo = _mm_unpacklo_epi16(vhl, _mm_setzero_si128());   // unpack sum of vh an vl into 2 x 4 x 32 bit vectors

    vsum = _mm_add_epi32(vsum, vhl_hi);
    vsum = _mm_add_epi32(vsum, vhl_lo);
  }
  // get sum of 4 x 32 bit partial sums
  vsum = _mm_add_epi32(vsum, _mm_srli_si128(vsum, 8));
  vsum = _mm_add_epi32(vsum, _mm_srli_si128(vsum, 4));
  result = _mm_cvtsi128_si32(vsum);

  // handle any residual bytes ( < 16)
  if (j < inner_length) {
    result += basic_newline_counter(&s0[j], inner_length - j);
  }
  return result;
}

long long count_newlines(const char *s, long long size);

long long count_newlines_bigfile(const char *s, long long size) {
  long long result = 0;
  char has_extra = (size % (INT_MAX / 2) ? 1 : 0);
  long long num_iters = (size / (INT_MAX / 2)) + has_extra;
  int i;
  for (i = 0; i < num_iters; ++i) {
    long long block_size = (i == (num_iters - 1)) && has_extra ? size % (INT_MAX / 2) : (INT_MAX / 2);
    result += fast_count_newlines_block((s + ((INT_MAX / 2) * i)), (int)block_size);
  }
  return result;
}

long long count_newlines(const char *s, long long size) {
  if (size > INT_MAX) {
    return count_newlines_bigfile(s, size);
  }
  return fast_count_newlines_block(s, (int)size);
}

#else // __SSE2__

#pragma message "Unable to use SSE; falling back onto serial method."

long long count_newlines(const char *s, long long size) {
  return basic_newline_counter(s, size);
}

#endif // __SSE2__

int main(int argc, char *argv[]) {
  const char *file_contents;
  int fd;
  struct stat sb;

  if (argc != 2) {
    fprintf(stderr, "Usage: ggflc file-name\n");
    return 1;
  }

  fd = open(argv[1], O_RDONLY);
  fstat(fd, &sb);

  file_contents = mmap(NULL, sb.st_size, PROT_READ, MAP_SHARED, fd, 0);
  if (file_contents == MAP_FAILED) {
    fprintf(stderr, "Failed top allocate %ld bytes of memory. Error code: %d\n", sb.st_size, errno);
    return 2;
  }
  if (madvise((void*)file_contents, sb.st_size, MADV_SEQUENTIAL)) {
    fprintf(stderr, "madvise() failed with error: %d", errno);
    return 2;
  }

  long long num_lines = count_newlines(file_contents, (long long)sb.st_size);
  printf("%s\t%lld\n", argv[1], num_lines);
  return 0;
}
